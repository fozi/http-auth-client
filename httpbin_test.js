const Auth = require('./auth')

const http = require("http")
const { URL } = require('url')

;[
	{
		url: "http://httpbin.org/basic-auth/test/hello",
		username: "test",
		password: "hello",
	},
	{
		url: "http://httpbin.org/digest-auth/auth/test/hello/MD5",
		username: "test", 
		password: "hello",
		n: 3,
	},
	{
		url: "http://httpbin.org/digest-auth/auth/test/hello/SHA-256",
		username: "test",
		password: "hello",
		n: 3,
	},
	{
		url: "http://httpbin.org/digest-auth/auth-int/test/hello/SHA-256",
		username: "test",
		password: "hello",
		n: 3,
	},
	{
		url: "http://httpbin.org/digest-auth/auth/test/hello/SHA-512",
		username: "test", 
		password: "hello",
		n: 3,
	},
]

.reduce(async (prev, tt) => {
	await prev

	for(var i = 0; i < (tt.n || 1); i++) {
		var res = await request(tt.url, {
			headers: tt.auth ? {
				authorization: tt.auth.authorization(tt.method || "GET", new URL(tt.url).pathname)
			} : {}
		})

		if(res.statusCode == 401) {
			if(!tt.auth) tt.auth = Auth.create(res.headers['www-authenticate']).credentials(tt.username, tt.password)
			else if(tt.auth.update(res.headers['www-authenticate'])) tt.auth.credentials(tt.username, tt.password)
			res = await request(tt.url, { headers: { authorization: tt.auth.authorization(tt.method || "GET", new URL(tt.url).pathname) }})
		}

		var data = await fetchbody(res)
		console.log("Request to " + tt.url + " done: ", data)
	}
}, Promise.resolve())

.catch( err => {
	console.error("Error: ", err)
})


function request(url, options) {
	return new Promise((resolve, reject) => {
		console.log("Starting request to", url, options && options.headers && options.headers.authorization ? "with authorization: " + options.headers.authorization : '')
		var req = http.request(url, options, res => {
			console.log("Status:", res.statusCode)
			if(res.statusCode == 401 && res.headers['www-authenticate']) console.log('www-authenticate:', res.headers['www-authenticate'])
			resolve(res)
		})
		req.on('error', reject)
		if(options && options.body) req.write(body)
		req.end()
	})
}

function fetchbody(res) {
	return new Promise((resolve, reject) => {
		var data = ''
		res.on('data', chunk => {
			data += chunk
		})
		res.on('end', () => {
			resolve(data)
		})
		res.on('error', reject)
	})
}
