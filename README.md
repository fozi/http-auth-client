# HTTP Authentication Client

[![pipeline status](https://gitlab.com/fozi/http-auth-client/badges/master/pipeline.svg)](https://gitlab.com/fozi/http-auth-client/commits/master) [![coverage report](https://gitlab.com/fozi/http-auth-client/badges/master/coverage.svg)](https://gitlab.com/fozi/http-auth-client/commits/master)

This library parses the `www-Authenticate` request header(s) and provides the `Authorization` response header value.

Unlike other libraries it does not do the request itself so it is not tied to any particular request library.

It currently supports HTTP Basic and HTTP Digest authentication.

## Usage

TODO

- do a request using your favourite request library.
- get a 401 response
- get the `www-authenticate` header(s)

```javascript
const Auth = require('http-auth-client')

// parse request header
var challenges = Auth.parseHeaders(req.headers['www-authenticate'])
var auth = Auth.create(challenges)

// once you have the username and password
auth.credentials(username, password)

// do an authenticated request
req.set("Authorization", auth.authorization("GET", "/test"))
```

### Auth

TODO

### Basic

TODO

### Digest

TODO

## License

MIT

(C) Fozi
