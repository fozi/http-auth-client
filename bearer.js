class Bearer {
	constructor(params) {
		// Nothing
	}

	credentials(token) {
		this.token = token
		return this
	}

	authorization() {
		return this.type + " " + this.token
	}

	update(params) {
		// Always needs new credentials
		// But we don't delete the token
		return true
	}
}

Bearer.prototype.type = "Bearer"

module.exports = Bearer