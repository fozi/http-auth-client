const { expect } = require('chai')
const Basic = require('../basic')

describe("Basic", function() {
	it("set the realm", function() {
		const realm = "Test realm"
		var basic = new Basic({ realm })
		expect(basic.realm).to.equal(realm)
		expect(basic.type).to.equal("Basic")
	})

	it("set the charset", function() {
		const charset = "UTF-8"
		expect(new Basic({ charset }).charset).to.equal(charset)
		expect(new Basic().charset).to.be.empty
	})

	it("work (ascii)", function() {
		expect(new Basic().credentials("Aladdin", "OpenSesame").authorization()).to.equal("Basic QWxhZGRpbjpPcGVuU2VzYW1l")
		expect(new Basic().credentials("Aláddín", "OpεnΣeßαme°").authorization()).to.equal("Basic QWzhZGTtbjpPcLVuo2XfsW1lsA==")
	})

	it("work (utf8)", function() {
		const charset = "UTF-8"
		expect(new Basic({ charset }).credentials("Aladdin", "OpenSesame").authorization()).to.equal("Basic QWxhZGRpbjpPcGVuU2VzYW1l")
		expect(new Basic({ charset }).credentials("Aláddín", "OpεnΣeßαme°").authorization()).to.equal("Basic QWzDoWRkw61uOk9wzrVuzqNlw5/OsW1lwrA=")
	})

	it("complain on no credentials", function() {
		expect(() => { new Basic().authorization() }).to.throw()
		expect(() => { new Basic({ realm: "test" }).authorization() }).to.throw()
		expect(() => { new Basic({ charset: "UTF-8" }).authorization() }).to.throw()
		expect(() => { new Basic({ realm: "test", charset: "UTF-8" }).authorization() }).to.throw()
	})

	it("update from string", function() {
		var basic = new Basic().credentials("a", "b")
		expect(basic.update(`Basic realm="testrealm@host.com"`)).to.be.true
		expect(basic.realm).to.equal("testrealm@host.com")
		expect(() => { basic.authorization() }).to.throw()
		expect(basic.credentials("a", "b").authorization()).to.equal("Basic YTpi")
	})

	it("update from object", function() {
		var basic = new Basic().credentials("a", "b")
		const parsed = {
			type: "Basic",
			params: {
				realm: "testrealm@host.com",
				charset: "UTF-8"
			}
		}
		expect(basic.update(parsed)).to.be.true
		expect(basic.realm).to.equal("testrealm@host.com")
		expect(() => { basic.authorization() }).to.throw()

		parsed.params = {
			charset: "ascii"
		}

		expect(basic.update(parsed.params)).to.be.true
		expect(basic.realm).to.equal("testrealm@host.com")
		expect(() => { basic.authorization() }).to.throw()
		expect(basic.credentials("a", "b").authorization()).to.equal("Basic YTpi")
	})

	it("updates from multiple choices", function() {
		var basic = new Basic().credentials("a", "b")
		expect(basic.update(`Test adjrendk~df65ttgfre/+=, Digest algorithm="SHA-256", realm="testrealm@host.com", nonce=dcd98b7102dd2f0e8b11d0f600bfb0c093, qop=auth, Basic realm="basictest@host.com", Digest algorithm=MD5, realm="testrealm@host.com", nonce=dcd98b7102dd2f0e8b11d0f600bfb0c093, qop=auth`)).to.be.true
		expect(basic.realm).to.equal("basictest@host.com")
		expect(() => { basic.authorization() }).to.throw()
	})

	it("update throws if basic is not an option", function() {
		var basic = new Basic().credentials("a", "b")
		expect(()=> basic.update(`Test adjrendk~df65ttgfre/+=, Digest algorithm="SHA-256", realm="testrealm@host.com", nonce=dcd98b7102dd2f0e8b11d0f600bfb0c093, qop=auth, Digest algorithm=MD5, realm="testrealm@host.com", nonce=dcd98b7102dd2f0e8b11d0f600bfb0c093, qop=auth`)).to.throw()
	})
})