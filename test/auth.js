const { expect } = require('chai')
const Auth = require('../auth')

const headers = [
	'Basic realm="testrealm@host.com"',
	'Digest algorithm="MD5", realm="testrealm@host.com", nonce=dcd98b7102dd2f0e8b11d0f600bfb0c093, qop=auth, foo=bar',
	'Digest algorithm="SHA-256", realm="testrealm@host.com", nonce=dcd98b7102dd2f0e8b11d0f600bfb0c093, qop=auth, foo=bar',
	'Test fooBar012=, Bearer discovery="https://www.example.com/.well-known/openid-configuration",valid="$#@"',
]

const allHeaders = `HTTP/1.1 401 Unauthorized
Content-type: text/html
Content-length: 0
${headers.map(h => 'www-Authenticate: ' + h).join('\n')}
X-Server: Fozi

`.replace(/\r?\n/g, "\r\n")

const parsed = [
	{
		name: "Basic",
		params: {
			realm: "testrealm@host.com",
		}
	},
	{
		name: "Digest",
		params: {
			algorithm: "MD5",
			realm: "testrealm@host.com",
			nonce: "dcd98b7102dd2f0e8b11d0f600bfb0c093",
			qop: "auth",
			foo: "bar"
		}
	},
	{
		name: "Digest",
		params: {
			algorithm: "SHA-256",
			realm: "testrealm@host.com",
			nonce: "dcd98b7102dd2f0e8b11d0f600bfb0c093",
			qop: "auth",
			foo: "bar"
		}
	},
	{
		name: "Test",
		params: "fooBar012="
	},
	{
		name: "Bearer",
		params: {
			discovery: "https://www.example.com/.well-known/openid-configuration",
			valid: "$#@"
		}
	},
]

describe("Auth", function() {
	describe("parseHeaders", function() {
		describe("can parse one header", function() {
			[
				{ header: headers[0], parsed: [parsed[0]] },
				{ header: headers[1], parsed: [parsed[1]] },
				{ header: headers[2], parsed: [parsed[2]] },
				{ header: headers[3], parsed: [parsed[3], parsed[4]] },
			].forEach(({header, parsed}) => {
				it(header, function() {
					expect(Auth.parseHeaders(header)).to.deep.equal(parsed)
				})
			})
		})

		it("can parse multiple headers", function(){
			expect(Auth.parseHeaders(...headers)).to.deep.equal(parsed)
		})

		it("can parse all headers", function() {
			expect(Auth.parseHeaders(allHeaders)).to.deep.equal(parsed)
		})
	})

	describe("pick", function() {
		it("works with challenges", function() {
			expect(Auth.pick(parsed)).to.deep.equal([parsed[2], parsed[1], parsed[0]])
		})
		it("works with all headers", function() {
			expect(Auth.pick(allHeaders)).to.deep.equal([parsed[2], parsed[1], parsed[0]])
		})
		it("works with a header", function() {
			expect(Auth.pick(headers.join(', '))).to.deep.equal([parsed[2], parsed[1], parsed[0]])
		})
	})

	describe("create", function() {
		describe("creates from typeList", function() {
			[ Auth.Basic, Auth.Digest, Auth.Bearer ].forEach(t => {
				it(t.prototype.type, function() {
					var auth = Auth.create(allHeaders, [t])
					expect(auth).to.be.an.instanceOf(t)
				})
			})
		})

		it("creates Basic", function() {
			var auth = Auth.create(parsed[0])
			expect(auth).to.be.an.instanceOf(Auth.Basic)
			expect(auth.realm).to.equal(parsed[0].params.realm)
		})

		it("creates Digest", function() {
			var auth = Auth.create(parsed[2])
			expect(auth).to.be.an.instanceOf(Auth.Digest)
			expect(auth.algorithm).to.equal(parsed[2].params.algorithm)
		})

		it("creates Bearer", function() {
			var auth = Auth.create(parsed[4], [ Auth.Bearer ])
			expect(auth).to.be.an.instanceOf(Auth.Bearer)
		})

		it("rejects invalid challenges", function() {
			expect(() => Auth.create()).to.throw()
			expect(() => Auth.create({})).to.throw()
			expect(() => Auth.create({ name: "test" })).to.throw()
			expect(() => Auth.create({ params: {}})).to.throw()
			expect(() => Auth.create(parsed[0], [ Auth.Digest ])).to.throw()
		})
	})
})