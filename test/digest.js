const { expect } = require('chai')
const Digest = require('../digest')
const Auth = require('../auth')

const Crypto = require('crypto')

function sha256(data) {
	return Crypto.createHash('sha256').update(data).digest()
}

function parse(response, options) {
	response = Auth.parseHeaders(response)
	return response[0]
}

function check(response, method, password) {
	var a1 = sha256(response.username + ":" + response.realm + ":" + password).toString('hex')
	var a2 = sha256(method + ":" + response.uri).toString('hex')
	var resp = sha256(a1 + ":" + response.nonce + ":" + response.nc + ":" + response.cnonce + ":" + response.qop + ":" + a2).toString('hex')
	return response.response == resp
}

const test_header = `Digest algorithm="SHA-256", realm="testrealm@host.com", nonce=dcd98b7102dd2f0e8b11d0f600bfb0c093, qop=auth`
const stale_header = `Digest algorithm="SHA-256", realm="testrealm@host.com", nonce=dcd98b7102dd2f0e8b11d0f600bfb0c094, stale=true, qop=auth`

describe("Digest", function() {
	it("works", function() {
		var response = new Digest(Auth.parseHeaders(test_header)[0]).credentials("Mufasa", "Circle Of Life").authorization("GET", '/test')
		expect(response).to.be.not.empty
		var resp = parse(response)

		expect(resp).to.have.property("name", "Digest")
		expect(resp.params).to.have.property("username", "Mufasa")
		expect(resp.params).to.have.property("realm", "testrealm@host.com")
		expect(resp.params).to.have.property("nonce", "dcd98b7102dd2f0e8b11d0f600bfb0c093")
		expect(resp.params).to.have.property("algorithm", "SHA-256")

		expect(check(resp.params, "GET", "Circle Of Life"), "Response validity").to.be.true
		expect(check(resp.params, "GET", "Circle Of Life"), "Response validity").to.be.true
		expect(check(resp.params, "GET", "Circle Of Life"), "Response validity").to.be.true
	})

	it("handles expored nonce", function() {
		var digest = new Digest(Auth.parseHeaders(test_header)[0]).credentials("Mufasa", "Circle Of Life")
		var response = parse(digest.authorization("GET", '/test'))
		expect(check(response.params, "GET", "Circle Of Life"), "Response validity").to.be.true

		expect(digest.update(stale_header)).to.be.false
		response = parse(digest.authorization("GET", '/test'))
		expect(check(response.params, "GET", "Circle Of Life"), "Response validity").to.be.true
	})
})