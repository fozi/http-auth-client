const Bearer = require('../bearer')
const { expect } = require('chai')

describe("Bearer", function() {
	it("works", function() {
		expect(new Bearer().credentials("test").authorization()).to.equal("Bearer test")
		expect(new Bearer().type).to.equal("Bearer")
	})

	it("updates", function() {
		var bearer = new Bearer()
		expect(bearer.update()).to.be.true
		expect(bearer.credentials("test").authorization()).to.equal("Bearer test")
	})

	xit("update throws if bearer is not an option", function() {
		expect(()=> new Bearer().update(`Digest algorithm="SHA-256", realm="testrealm@host.com", nonce=dcd98b7102dd2f0e8b11d0f600bfb0c093, qop=auth, Basic realm="basictest@host.com"`)).to.throw()
	})
})